import torch
from torch_geometric.data import DataLoader
from models import GCN


class GcnLearning:
    def run(self, dataset, epochs=100, train_perc=0.8, batch_size=64, seed=12345):
        torch.manual_seed(seed)
        dataset = dataset.shuffle()
        train_dataset = dataset[:int(len(dataset) * train_perc)]
        test_dataset = dataset[int(len(dataset) * train_perc):]
        train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
        test_loader = DataLoader(test_dataset, batch_size=batch_size, shuffle=False)
        model = GCN(hidden_channels=64, dataset=dataset, seed=seed)
        optimizer = torch.optim.Adam(model.parameters(), lr=0.01)
        criterion = torch.nn.CrossEntropyLoss()
        logs = {}
        for epoch in range(epochs):
            self.train(model, train_loader, criterion, optimizer)
            train_acc = self.test(model, train_loader)
            test_acc = self.test(model, test_loader)
            print(f'Epoch: {epoch:03d}, Train Acc: {train_acc:.4f}, Test Acc: {test_acc:.4f}')
            logs[epoch] = {'train_acc': train_acc, 'test_acc': test_acc}
        return logs

    def train(self, model, train_loader, criterion, optimizer) -> torch.Tensor:
        model.train()
        combined_logits = torch.Tensor()
        for data in train_loader:  # Iterate in batches over the training dataset.
            logits = model(data.x, data.edge_index, data.batch)  # Perform a single forward pass.
            loss = criterion(logits, data.y)  # Compute the loss.
            loss.backward()  # Derive gradients.
            optimizer.step()  # Update parameters based on gradients.
            optimizer.zero_grad()  # Clear gradients.
            combined_logits = torch.cat((combined_logits, logits))
        return combined_logits

    def test(self, model, loader) -> float:
        model.eval()
        correct = 0
        for data in loader:  # Iterate in batches over the training/test dataset.
            logits = model(data.x, data.edge_index, data.batch)
            pred = logits.argmax(dim=1)  # Use the class with highest probability.
            correct += int((pred == data.y).sum())  # Check against ground-truth labels.
        return correct / len(loader.dataset)  # Derive ratio of correct predictions.
